/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Yair Augusto GutiÃ©rrez Fosado
                         Davide Michieletto
                         Filippo Conforto
------------------------------------------------------------------------- */

#ifdef FIX_CLASS
FixStyle(topoII, FixTopo2)

#else

#ifndef LMP_FIX_TOPOII_H
#define LMP_FIX_TOPOII_H

#include "fix.h"
#include "compute.h"
#include "pair.h"
#include "compute_chunk_atom.h"
#include "fix_ave_chunk.h"

namespace LAMMPS_NS
{

   class FixTopo2 : public Fix
   {
   public:
      FixTopo2(class LAMMPS *, int, char **);
      ~FixTopo2() override;
      int setmask() override;
      void init() override;
      void post_integrate() override;
      void write_restart(FILE *) override;
      void restart(char *) override;
      double compute_vector(int) override;
      double compute_array(int, int) override;
      double compute_scalar() override;

      void maxdensid();
      void smcfront();
      void changetype();
      void removetopo(long);
      void placetopo(long);

   private:
      int flagtopo, nevery, seed, ntopo, ltopo, lpol, nsmc, topotype, tsteptopo, intertype, lensel, chunksize, nchunk;
      int *dens_counts;
      double kon, koff;
      bool debug;
      long *topoids, *sel, *locsel, *smclpos;
      class ComputeChunkAtom *comp_chunk;
      class FixAveChunk *dens_chunk;
      class NeighList *list;
      class RanPark *random_equal;
      class RanPark *random_unequal;
      class RanMars *random;
      class Fix *connFix;
      char *connFixName;
   };
}

#endif
#endif
