/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   https://lammps.sandia.gov/, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Yair Augusto GutiÃ©rrez Fosado
                         Davide Michieletto
                         Filippo Conforto
------------------------------------------------------------------------- */
#include "fix_topo2.h"

// The atom class provides access to various atom properties, including the atom type, molecular ID, position, velocity, and force
#include "atom.h"

// Came by default in fix_swap_atom
#include <cmath>
#include <cctype>
#include <cfloat>
#include <cstring>
#include "atom.h"
#include "update.h"
#include "modify.h"
#include "fix.h"
#include "comm.h"
#include "compute.h"
#include "modify.h"
#include "group.h"
#include "domain.h"
#include "region.h"
#include "random_park.h"
#include "force.h"
#include "pair.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "kspace.h"
#include "memory.h"
#include "error.h"
#include "neighbor.h"

// Davide include them (with more)
#include "math.h"
#include "stdlib.h"
#include "string.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "random_mars.h"
#include "citeme.h"
#include <stdlib.h>
#include <time.h>  /* Important for random number generator */
#include <sstream> // std::stringstream
#include <fstream> //ofstream
#include <algorithm>
#include <iterator>

#include "input.h"
#include "variable.h"
#include <iostream>
#include <algorithm>

using namespace std;
using namespace LAMMPS_NS;
using namespace FixConst;

static const char cite_fix_topo2[] =
    "Fast Binding of Topoisomerase Accelerates Topological Decatenation but Slows DownSupercoil Relaxation:\n\n"
    "@Article{Battaglia2023,\n"
    " author = {Battaglia et al},\n"
    " title = {xx},\n"
    " journal = {xx},\n"
    " year =    2018,\n"
    " volume =  xx,\n"
    " pages =   {xx}\n"
    "}\n\n";

/* ---------------------------------------------------------------------- */
// Constructor: The name of the class is FixTopo2 and the fix-style-name to
// be used in the lammps script is fix_topo2.
FixTopo2::FixTopo2(LAMMPS *lmp, int narg, char **arg) : Fix(lmp, narg, arg),
                                                        debug(0), list(nullptr), random_equal(nullptr), random_unequal(nullptr), random(nullptr), dens_chunk(nullptr), comp_chunk(nullptr), dens_counts(nullptr), sel(nullptr), locsel(nullptr), connFix(nullptr)
{
    // Cite
    if (lmp->citeme)
        lmp->citeme->add(cite_fix_topo2);

    // Check that the atom_style is molecular
    if (atom->molecular != 1)
        error->all(FLERR, "Cannot use fix_topo2 with non-molecular systems");

    // Number of arguments for the fix. The first three arguments are parsed by Fix base class constructor.
    // The rest are specific to this fix. From 4 to 8 are mandatory for all cases.
    // 4. flagtopo: static or randjump or jump2smc or jump2maxdens
    //    1. static: topoII proteins are placed randomly along the polymers
    //    2. randjump: topoII proteins jump randomly in the system
    //    3. jump2smc: topoII proteins jump in front of loop extruding proteins
    //    4. jump2maxdens: topoII proteins jump in the system chunk with maximum density
    // 5. ntopo: number of topoII proteins in the system.
    // 6. ltopo: length of each topoII protein segment in the system.
    // 7. topotype: type of the beads of which topoII segments are formed by
    // 8. seed: seed for random number generation
    // 9. lpol: length of ring or linear polymers in the simulated system
    // If static we need one more argument:
    // 10. tsteptopo: at which timestep we want to introduce the topoIIs
    // If randjump or jump2smc or jump2maxdens we need instead:
    // 10. nevery: how many timesteps are simulated between two jumps attempts
    // 11. kon: probability to load an unloaded topoII
    // 12. koff: probability to unload a loaded topoII
    // 13. intertype: bead type of topoII segments after a successful jump to be preserved until next iteration of the fix
    // If jump2maxdens, we need two more
    // 13. chunksize: chunksize for density computation
    // (optional)
    // - FixID: name of the fix used to implement loop extrusion

    force_reneighbor = 1;
    next_reneighbor = -1;
    restart_global = 1;
    vector_flag = 1;
    size_vector = 2;
    global_freq = 1;
    extvector = 0;

    // Assigning flagtopo value
    flagtopo = 0;
    if (strcmp(arg[3], "static") == 0)
    {
        flagtopo = 1;
    }
    else if (strcmp(arg[3], "randjump") == 0)
    {
        flagtopo = 2;
    }
    else if (strcmp(arg[3], "jump2smc") == 0)
    {
        flagtopo = 3;
    }
    else if (strcmp(arg[3], "jump2maxdens") == 0)
    {
        flagtopo = 4;
    }
    else
    {
        error->all(FLERR, "Illegal fix topo2 command");
    }

    // Check that the number of arguments is the correct one
    switch (flagtopo)
    {
    case 1:
    {
        if ((narg != 10) && (narg != 11))
        {
            error->all(
                FLERR,
                "Illegal fix topo2 command, a different number of arguments is required for static");
        }
        break;
    }
    case 2:
    {
        if ((narg != 12) && (narg != 13))
        {
            error->all(FLERR,
                       "Illegal fix topo2 command, a different number of arguments is required for "
                       "randjump");
        }
        break;
    }
    case 3:
    {
        if ((narg != 13))
        {
            error->all(FLERR,
                       "Illegal fix topo2 command, a different number of arguments is required for "
                       "jump2smc");
        }
        break;
    }
    case 4:
    {
        if ((narg != 13) && (narg != 14))
        {
            error->all(FLERR,
                       "Illegal fix topo2 command, a different number of arguments is required for "
                       "jump2maxdens");
        }
        break;
    }

    default:
    {
        break;
    }
    }

    // Storing the number of topoII proteins to deploy
    ntopo = utils::inumeric(FLERR, arg[4], false, lmp);
    if (ntopo < 0 || ntopo > (atom->natoms))
        error->all(FLERR, "Illegal fix topo2 command: in ntopo");

    // Storing the length of topoII segments
    ltopo = utils::inumeric(FLERR, arg[5], false, lmp);
    int length = ntopo * ltopo;
    if (ltopo < 0 || length > atom->natoms)
        error->all(FLERR, "Illegal fix topo2 command: in ltopo");

    // Storing the type of beads forming topoII segments
    topotype = utils::inumeric(FLERR, arg[6], false, lmp);
    int ilo, ihi, jlo, jhi;
    utils::bounds(FLERR, arg[6], 1, atom->ntypes, ilo, ihi, error);

    seed = utils::inumeric(FLERR, arg[7], false, lmp);
    if (seed <= 0)
        error->all(FLERR, "Illegal fix topo2 command");

    // Length of polymers in simulation
    lpol = utils::inumeric(FLERR, arg[8], false, lmp);
    if (lpol <= 0)
        error->all(FLERR, "Illegal fix topo2 command");

    connFixName = new char[5];
    connFixName = "nofix";

    // For first option, we have to set the timestep at which we want to introduce topoII
    if (flagtopo == 1)
    {
        tsteptopo = utils::inumeric(FLERR, arg[9], false, lmp);
        if (tsteptopo < 0)
            error->all(FLERR, "Illegal fix topo2 command");
        if (tsteptopo < update->beginstep)
            error->all(FLERR, "Illegal fix topo2 command");
    }

    // In the remaining cases we need more arguments:
    else if (flagtopo >= 2)
    {
        // Saving the number of timesteps between two jump attempts
        nevery = utils::inumeric(FLERR, arg[9], false, lmp);
        if (nevery <= 0)
            error->all(FLERR, "Illegal fix topo2 command");

        // Saving the probability to be loaded
        kon = utils::numeric(FLERR, arg[10], false, lmp);
        if (kon < 0.0 || kon > 1.0)
            error->all(FLERR, "Illegal fix topo2 command");

        // Saving the probability to be loaded
        koff = utils::numeric(FLERR, arg[11], false, lmp);
        if (koff < 0.0 || koff > 1.0)
            error->all(FLERR, "Illegal fix topo2 command");

        // Saving the bead type for topo2 segments after a move
        intertype = utils::inumeric(FLERR, arg[12], false, lmp);
        utils::bounds(FLERR, arg[12], 1, atom->ntypes, ilo, ihi, error);

        if (flagtopo == 4)
        {
            chunksize = utils::numeric(FLERR, arg[13], false, lmp);
            if (chunksize < 0)
            {
                error->all(FLERR, "Illegal fix topo2 command");
            }

            if (narg == 15)
            {
                connFixName = new char[static_cast<int>(sizeof(arg[14]) / sizeof(char))];
                std::copy(arg[14], arg[14] + static_cast<int>(sizeof(arg[14]) / sizeof(char)), connFixName);
            }
        }

        else if (narg == 14)
        {
            connFixName = new char[static_cast<int>(sizeof(arg[13]) / sizeof(char))];
            std::copy(arg[13], arg[13] + static_cast<int>(sizeof(arg[13]) / sizeof(char)), connFixName);
        }
    }

    else if (narg == 11)
    {
        connFixName = new char[static_cast<int>(sizeof(arg[10]) / sizeof(char))];
        std::copy(arg[10], arg[10] + static_cast<int>(sizeof(arg[10]) / sizeof(char)), connFixName);
    }

    // Initialize the topo2 array
    topoids = new long[ntopo];

    for (int i = 0; i < ntopo; i++)
    {
        topoids[i] = atom->natoms + 1;
    }

    // random number generator, same for all procs
    random_equal = new RanPark(lmp, seed);

    // random number generator, not the same for all procs
    random_unequal = new RanPark(lmp, seed);

    // initialize Marsaglia RNG with processor-unique seed
    random = new RanMars(lmp, seed + comm->me);

    // To get a different random number every time the program is executed
    srand(time(NULL) * seed);
}

/****************/
/*  Destructor  */
/****************/
FixTopo2::~FixTopo2()
{
    delete random_equal;
    delete random_unequal;
    delete random;
    delete dens_chunk;
    delete comp_chunk;
    delete topoids;

    memory->destroy(list);
    memory->destroy(sel);
    memory->destroy(locsel);
    memory->destroy(dens_counts);
}

/**************/
/*  Set mask  */
/**************/
// It determines the stage in the timestep at which the fix will be executed.
// The post integrate method is set for operations that need to happen immediately after updates by for example fix nve which performs the start-of-timestep velocity-Verlet integration operations to update velocities by a half-step, and coordinates by a full step. Only a few fixes use this, e.g. to reflect particles off box boundaries.
int FixTopo2::setmask()
{
    int mask = 0;
    mask |= POST_INTEGRATE;
    return mask;
}

/***********************/
/*  The init() method  */
/***********************/
// This is called at the beginning of each run, simply sets some internal flags
void FixTopo2::init()
{
    // require an atom style with molecule IDs
    if (atom->molecule == NULL)
        error->all(FLERR, "Must use atom style with molecule IDs with fix topo2");

    // pair and bonds must be defined
    if (force->pair == NULL || force->bond == NULL)
        error->all(FLERR, "Fix topo2 requires pair and bond styles");

    // angles must be defined
    if (force->angle == NULL && atom->nangles > 0 && comm->me == 0)
        error->warning(FLERR, "Fix topo2 will ignore defined angles");

    if (force->pair->single_enable == 0)
        error->all(FLERR, "Pair style does not support fix topo2");

    // no dihedral or improper potentials allowed
    if (force->dihedral || force->improper)
        error->all(FLERR, "Fix topo2 cannot use dihedral or improper styles");

    // special bonds must be 0 1 1
    if (force->special_lj[1] != 0.0 || force->special_lj[2] != 1.0 ||
        force->special_lj[3] != 1.0)
        error->all(FLERR, "Fix topo2 requires special_bonds = 0,1,1");

    if (flagtopo == 4)
    {
        std::string chunk_str = std::to_string(chunksize);
        char chunk_arr[chunk_str.length() + 1];
        strcpy(chunk_arr, chunk_str.c_str());

        // Defining arguments necessary to deploy a compute for 3D binning (see https://docs.lammps.org/compute_chunk_atom.html for reference)
        char *argcomp[] = {"internal_chunk_atom", "all", "chunk/atom", "bin/3d",
                           "x",
                           "lower", chunk_arr, "y", "lower", chunk_arr, "z", "lower", chunk_arr};
        comp_chunk = (ComputeChunkAtom *)modify->add_compute(13, argcomp, 0);

        std::string nevery_str = std::to_string(nevery);
        char nevery_arr[nevery_str.length() + 1];
        strcpy(nevery_arr, nevery_str.c_str());

        // Defining arguments necessary to deploy a fix to compute local density of beads (see https://docs.lammps.org/fix_ave_chunk.html for reference)
        if (debug)
        {
            char *argfix[] = {"internal_ave_chunk", "all", "ave/chunk", "1", "1", nevery_arr, "internal_chunk_atom", "density/number", "file", "locdensity.data"};
            dens_chunk = (FixAveChunk *)modify->add_fix(10, argfix, 0);
        }
        else
        {
            char *argfix[] = {"internal_ave_chunk", "all", "ave/chunk", "1", "1", nevery_arr, "internal_chunk_atom", "density/number"};
            dens_chunk = (FixAveChunk *)modify->add_fix(8, argfix, 0);
        }

        // Calculate number of chunks and define a new array for counting number of atoms in each chunk
        nchunk = comp_chunk->compute_scalar();
        dens_counts = new int[nchunk];
    }

    else
    {
        dens_counts = nullptr;
    }

    // Store the connected Fix ID pointer
    if (strcmp(connFixName, "nofix") == 0)
    {
        connFix = nullptr;
    }
    else
    {
        connFix = modify->get_fix_by_id(connFixName);
        if (!connFix)
            error->all(FLERR, "Illegal ausiliary Fix");
    }

    /*
      // need a full neighbor list, built every Nevery steps
      int irequest = neighbor->request(this,instance_me);
      neighbor->requests[irequest]->pair = 0;
      neighbor->requests[irequest]->fix = 1;
      neighbor->requests[irequest]->half = 0;
      neighbor->requests[irequest]->full = 1;
      //neighbor->requests[irequest]->occasional = 1;
    */
}

// Then, an init_list() method needs to be introduced to get hold of the neighbor list pointer, with a placeholder named list
// void FixTopo2::init_list(int /*id*/, NeighList *ptr)
//{
//   list = ptr;
// }

/************************************************/
/*  The action of this fix is implemented here  */
/************************************************/
void FixTopo2::post_integrate()
{
    long idto;
    double rndnum, krand;
    int i = 0;
    int ns = nsmc;
    bool flag;

    // Static: TopoII chooses a random position and it stays there until the end of the simulation
    if (flagtopo == 1)
    {
        if (tsteptopo != update->ntimestep)
            return;

        while (i < ntopo)
        {
            if (comm->me == 0)
                idto = random_equal->uniform() * atom->natoms;

            MPI_Bcast(&idto, 1, MPI_LONG, 0, world);

            // Checking if there is any conflict with polymer borders
            if (idto % lpol <= ltopo)
            {
                continue;
            }

            if (connFix)
            {
                // Check for conflicts with connected fix
                for (int k = 0; k < connFix->compute_scalar(); k++)
                {
                    if (connFix->compute_array(k, 1) > atom->natoms)
                        continue;
                    if ((connFix->compute_array(k, 0) >= (idto - ltopo)) && (connFix->compute_array(k, 0) < (idto)))
                    {
                        flag = 1;
                        break;
                    }
                    if ((connFix->compute_array(k, 1) >= (idto - ltopo)) && (connFix->compute_array(k, 1) < (idto)))
                    {
                        flag = 1;
                        break;
                    }
                }
            }
        }

        placetopo(idto);
        i++;
    }

    if ((update->ntimestep % nevery) && (update->ntimestep != 1))
        return;

    // Change type from intertype to standard one
    changetype();

    if (flagtopo == 3)
        smcfront();
    if (flagtopo == 4)
        maxdensid();

    for (int i = 0; i < ntopo; i++)
    {
        flag = 1;

        if (comm->me == 0)
            krand = random_equal->uniform();
        MPI_Bcast(&krand, 1, MPI_LONG, 0, world);
        MPI_Barrier(world);

        if ((topoids[i] != atom->natoms + 1) && (krand < koff))
        {
            removetopo(topoids[i]);
            topoids[i] = atom->natoms + 1;
            continue;
        }
        else if (((topoids[i] == atom->natoms + 1) && (krand > kon)) || (topoids[i] != atom->natoms + 1))
            continue;

        while (flag)
        {

            flag = 0;
            if (comm->me == 0)
                rndnum = random_equal->uniform();

            MPI_Bcast(&rndnum, 1, MPI_LONG, 0, world);
            MPI_Barrier(world);

            switch (flagtopo)
            {

            // Random jump: with a certain frequency (nevery) the topoII can jump to a random position.
            // The decision of jumping is done with a certain probability.
            case 2:
            {
                idto = static_cast<int>(rndnum * atom->natoms);
                break;
            }

            // Jump in front of smc extruders if possible
            case 3:
            {
                if (ns > 0)
                {
                    // Choose randomly a bead between the left feet of SMCs
                    idto = smclpos[static_cast<int>(rndnum * (ns - 1))];
                    ns--;
                }
                else
                {
                    // Choose Randomly a bead [1, natoms]
                    idto = static_cast<int>(rndnum * atom->natoms);
                }
                break;
            }

            // Jump to maximum density: with a certain frequency (nevery) the topoII can jump to the position of maximum bead density.
            // The decision of jumping is done with a certain probability.
            case 4:
            {
                idto = sel[static_cast<int>(rndnum * lensel)];

                break;
            }

            default:
            {
                return;
            }
            }

            // Checking if there is any conflict with polymer borders
            if (idto % lpol <= ltopo)
            {
                flag = 1;
                continue;
            }

            // Check for conflicts for the newly generated positions
            for (int j = 0; j < i; j++)
            {
                if (((((idto - ltopo) < topoids[j]) && ((idto - ltopo) >= (topoids[j] - ltopo))) || (((idto) < topoids[j]) && ((idto) >= (topoids[j] - ltopo)))) || (topoids[j] == idto))
                {
                    flag = 1;
                    break;
                }
            }

            if (flag)
                continue;

            if (connFix)
            {
                // Check for conflicts with connected fix
                for (int k = 0; k < connFix->compute_scalar(); k++)
                {
                    if (connFix->compute_array(k, 1) < 0)
                        continue;
                    if ((connFix->compute_array(k, 0) >= (idto - ltopo)) && (connFix->compute_array(k, 0) < (idto)))
                    {
                        flag = 1;
                        break;
                    }
                    if ((connFix->compute_array(k, 1) >= (idto - ltopo)) && (connFix->compute_array(k, 1) < (idto)))
                    {
                        flag = 1;
                        break;
                    }
                }
            }
        }

        // Removing the new topoisomerases in the old positions
        if (topoids[i] != atom->natoms + 1)
            removetopo(topoids[i]);

        // Placing the new topoisomerases in the selected positions
        placetopo(idto);

        topoids[i] = idto;
        next_reneighbor = update->ntimestep;
    }

    return;
}

/*  -----------------------------------------------------*/
/*  Find the bead with maximum density (if flagtopo==3)  */
/*  -----------------------------------------------------*/
void FixTopo2::maxdensid()
{
    // Defining an array for newly selected tags
    long *seltags = new long[ntopo];

    memory->destroy(dens_counts);
    memory->create(dens_counts, nchunk, "FixTopo2::maxdensid()");
    int *nlarray = nullptr;
    int *disp = nullptr;

    int nlocal = atom->nlocal;
    long loccnt = 0;
    int locnocnts = 0;
    int nocnts = 0;

    // Trigger chunk density calculation
    dens_chunk->end_of_step();
    for (size_t i = 0; i < nchunk; i++)
    {
        // Copy the density of each chunk
        dens_counts[i] = dens_chunk->compute_array(i, 3);
    }

    // Find the chunk with maximum density
    int maxdenschunk = std::max_element(dens_counts, dens_counts + nchunk) - dens_counts + 1;

    // Initializing local selected and global selected arrays
    memory->destroy(locsel);
    memory->create(locsel, atom->nlocal, "FixTopo2::maxdensid()");
    memory->destroy(sel);
    memory->create(sel, atom->natoms, "FixTopo2::maxdensid()");

    // Store in each processor the atom contained in the chunk with maximum density
    for (int i = 0; i < nlocal; i++)
    {
        if (comp_chunk->ichunk[i] == maxdenschunk)
        {
            locsel[loccnt++] = atom->tag[i];
        }
    }

    // Store information if there is no atom of chosen chunk in the processor
    if (loccnt == 0)
    {
        locnocnts = 1;
        locsel[loccnt++] = 0;
    }

    MPI_Barrier(world);

    // Define an array which entries are the number (nlocal), of atoms in each processor
    memory->destroy(nlarray);
    memory->create(nlarray, comm->nprocs, "FixTopo2::maxdensid()");

    MPI_Allgather(&loccnt, 1, MPI_INT, nlarray, 1, MPI_INT, MPI_COMM_WORLD);

    lensel = 0;
    // Space between two consecutive entries
    int space = 0;
    // Define an array which contains the displacement between entries, when using MPI_Gatherv.
    memory->destroy(disp);
    memory->create(disp, comm->nprocs, "FixTopo2::maxdensid()");

    // Compute final array dispositions
    disp[0] = 0;
    lensel += nlarray[0];
    for (int i = 1; i < comm->nprocs; i++)
    {
        lensel += nlarray[i];
        disp[i] = disp[i - 1] + nlarray[i - 1] + space;
    }

    // Calculate and share the number of processors with zero counts
    MPI_Allreduce(&locnocnts, &nocnts, 1, MPI_LONG, MPI_SUM, world);

    // Gather and share the array containing the tags of selected atoms
    MPI_Allgatherv(locsel, loccnt, MPI_LONG, sel, nlarray, disp, MPI_LONG, world);

    // Sort and reduce lensel to exlude 0 counts coming from processors with 0 chunk atoms
    std::sort(sel, sel + lensel, greater<int>());
    lensel -= nocnts;

    memory->destroy(dens_counts);
    memory->destroy(locsel);
}

/*  ----------------------------------------------------------------------------------*/
/*  Positioning the topo2 in front of the smcs  or randomly if all the smcs are busy */
/*  ---------------------------------------------------------------------------------*/
void FixTopo2::smcfront()
{
    nsmc = connFix->compute_scalar();

    // Gather smc positions
    for (int k = 0; k < nsmc; k++)
    {
        smclpos[k] = connFix->compute_array(k, 0);
    }
}

/*  ----------  */
/*  Place topo  */
/*  ----------  */
void FixTopo2::placetopo(long id)
{
    int m;
    int idl;

    // Changing type accordingly to new position
    for (int i = 0; i < ltopo; i++)
    {
        idl = atom->map(id - ltopo + i);
        if (((m = idl) >= 0) && (idl < atom->nlocal))
        {
            atom->type[m] = topotype;
        }
    }

    MPI_Barrier(world);
}

/*  ------------------------  */
/*  Change type intermediate  */
/*  ------------------------  */
void FixTopo2::removetopo(long id)
{
    // The number  of owned atoms belonging to the core that executes this line
    int nlocal = atom->nlocal;

    int m;
    int idl;

    // Change the type of atoms from (topotype) to (intertype)
    for (int i = 0; i < ltopo; i++)
    {
        idl = atom->map(id - ltopo + i);
        if (((m = idl) >= 0) && (idl < atom->nlocal))
        {
            atom->type[m] = intertype;
        }
    }
}

/*  ------------------------  */
/*  Change type intermediate  */
/*  ------------------------  */
void FixTopo2::changetype()
{
    // The number  of owned atoms belonging to the core that executes this line
    int nlocal = atom->nlocal;

    int m;
    int idl;

    // Change the type of atoms from intertype to 1
    for (int l = 0; l < nlocal; l++)
    {
        if (atom->type[l] == intertype)
            atom->type[l] = 1;
    }
}

/***********************************************************************/
/* Needed to write a restart file that can continue with the simulation*/
/***********************************************************************/
void FixTopo2::write_restart(FILE *fp)
{
    int n = 0;
    long rlist[5 + ntopo];
    rlist[n++] = random_equal->state();
    rlist[n++] = random_unequal->state();
    rlist[n++] = next_reneighbor;
    rlist[n++] = update->ntimestep;

    rlist[n++] = ntopo;

    for (int i = 0; i < ntopo; i++)
    {
        rlist[n++] = topoids[i];
    }

    if (comm->me == 0)
    {
        int size = n * sizeof(long);
        fwrite(&size, sizeof(int), 1, fp);
        fwrite(rlist, sizeof(long), n, fp);
    }
}

/* ----------------------------------------------------------------------
   use state info from restart file to restart the Fix
------------------------------------------------------------------------- */
void FixTopo2::restart(char *buf)
{
    int n = 0;
    long *rlist = (long *)buf;

    seed = static_cast<int>(rlist[n++]);
    random_equal->reset(seed);

    seed = static_cast<int>(rlist[n++]);
    random_unequal->reset(seed);

    next_reneighbor = static_cast<bigint>(rlist[n++]);

    bigint ntimestep_restart = static_cast<bigint>(rlist[n++]);
    if (ntimestep_restart != update->ntimestep)
        error->all(FLERR, "Must not reset timestep when restarting fix topo2");

    int ntopo_rest = rlist[n++];
    if (ntopo_rest != ntopo)
        error->all(FLERR, "Invalid restart, number of topo2 has changed!");

    for (int i = 0; i < ntopo; i++)
    {
        topoids[i] = static_cast<long>(rlist[n++]);
    }
}

/*
Returns number of topos
*/
double FixTopo2::compute_scalar()
{
    return ntopo;
}

/*
Returns position of i topo
*/
double FixTopo2::compute_vector(int i)
{
    return topoids[i];
}

/*
Returns position of lower and upper bounds of topo2
*/
double FixTopo2::compute_array(int i, int flag)
{

    if (flag)
    {
        return topoids[i];
    }
    else
    {
        return topoids[i] - ltopo;
    }
}