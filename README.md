# TopoII Fix for LAMMPS

# Introduction

The following repository contains source code for LAMMPS Topo2 simulation module inspired by a previous implementation available at (source [here](https://git.ecdf.ed.ac.uk/ygutier2/topo2-lammps-implementation.git)), with detailed discussion in the following publication: https://doi.org/10.1093/nar/gkac260. Development started in 11/22 and based on LAMMPS(23 Jun 2022 - Update 3)(source [here](https:-github.com/lammps/lammps/commit/88c8b6ec6feac6740d140393a0d409437f637f8b)).

The goal of this code is to introduce an easy and parallelised way to deploy and run topoisomerase driven topological action on polymers. This is modelled as short segments which beads interact with any other beads through a soft repulsive potential instead of a Lennard-Jones one (see complete discussion in publication)

# Repository structure

- initfiles: contains sample datafiles to run tests
- exec: contains sample LAMMPS and bash scripts
- src: contain source files for fix_topo2 code

# How to run Fix SMC

The fix can be added to a regular LAMMPS script, by adding the following line with adequate parameters.

fix "fixname" "considered beads" topoII "par1" "par2" "par3" "par4" "par5" "par6" "par7" "par8" "par9" "par10" "par11"

1. flagtopo: static or randjump or jump2smc or jump2maxdens
   1. static: topoII proteins are placed randomly along the polymers
   2. randjump: topoII proteins jump randomly in the system
   3. jump2smc: topoII proteins jump in front of loop extruding proteins
   4. jump2maxdens: topoII proteins jump in the system chunk with maximum density
2. ntopo: number of topoII proteins in the system.
3. ltopo: length of each topoII protein segment in the system.
4. topotype: type of the beads of which topoII segments are formed by
5. seed: seed for random number generation
6. lpol: length of ring or linear polymers in the simulated system
   If static we need one more argument:
7. tsteptopo: at which timestep we want to introduce the topoIIs
   If randjump or jump2smc or jump2maxdens we need instead:
8. nevery: how many timesteps are simulated between two jumps attempts
9. kon: loading probability for unloaded topoII
10. koff: unloading probability for loaded topoII
11. intertype: bead type of topoII segments after a successful jump to be preserved until next iteration of the fix
    If jump2maxdens, we need two more
12. chunksize: chunksize for density computation
    (optional)

- FixID: name of the fix used to implement loop extrusion
